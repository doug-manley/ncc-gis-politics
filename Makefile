all: ncc-gis-politics-csv ncc-gis-politics-csv.app ncc-gis-politics-csv.exe

export CGO_ENABLED := 0

.PHONY: clean

clean:
	rm -f ncc-gis-politics-csv ncc-gis-politics-csv.app ncc-gis-politics-csv.exe

ncc-gis-politics-csv: $(wildcard cmd/ncc-gis-politics-csv/*.go internal/**/*.go)
	GOOS=linux go build -o $@ cmd/ncc-gis-politics-csv/*.go

ncc-gis-politics-csv.app: $(wildcard cmd/ncc-gis-politics-csv/*.go internal/**/*.go)
	GOOS=darwin go build -o $@ cmd/ncc-gis-politics-csv/*.go

ncc-gis-politics-csv.exe: $(wildcard cmd/ncc-gis-politics-csv/*.go internal/**/*.go)
	GOOS=windows go build -o $@ cmd/ncc-gis-politics-csv/*.go

