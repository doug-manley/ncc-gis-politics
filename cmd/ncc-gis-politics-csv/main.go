package main

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"strings"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

func main() {
	var debug bool
	rootCmd := cobra.Command{
		Use: "ncc-gis-politics-csv",
		PersistentPreRun: func(cmd *cobra.Command, args []string) {
			if debug {
				logrus.SetLevel(logrus.DebugLevel)
			}
		},
	}
	rootCmd.PersistentFlags().BoolVar(&debug, "debug", false, "Enable this to show more verbose logging.")

	{
		var addressFormat string
		var addressQuality float64
		var limit int
		var lookupCoordinates bool
		var lookupRD bool
		var lookupSD bool

		latitudeColumn := "latitude"
		longitudeColumn := "longitude"
		rdColumn := "rd"
		sdColumn := "sd"
		cmd := &cobra.Command{
			Use:   "lookup",
			Short: "Lookup address data",
			Args:  cobra.ExactArgs(1),
			Run: func(cmd *cobra.Command, args []string) {
				filename := args[0]
				logrus.Debugf("Filename: %s", filename)

				if lookupRD || lookupSD {
					lookupCoordinates = true
				}

				logrus.Debugf("Lookup coodinates: %t", lookupCoordinates)
				logrus.Debugf("Lookup RD: %t", lookupRD)
				logrus.Debugf("Lookup SD: %t", lookupSD)

				fileHandle, err := os.Open(filename)
				if err != nil {
					logrus.Errorf("Error: [%T] %v", err, err)
					os.Exit(1)
				}
				defer fileHandle.Close()
				csvReader := csv.NewReader(fileHandle)
				rows, err := csvReader.ReadAll()
				if err != nil {
					logrus.Errorf("Error: [%T] %v", err, err)
					os.Exit(1)
				}
				if len(rows) == 0 {
					logrus.Errorf("CSV is missing header row.")
					os.Exit(1)
				}
				header := rows[0]
				rows = rows[1:]

				if limit > 0 && len(rows) > limit {
					rows = rows[:limit]
				}

				headerIndexMap := map[string]int{}
				for c, name := range header {
					headerIndexMap[name] = c
				}

				extraColumnMap := map[string]bool{}
				if lookupCoordinates {
					if addressFormat == "" {
						logrus.Errorf("Missing address format.")
						os.Exit(1)
					}
					if _, ok := headerIndexMap[latitudeColumn]; !ok {
						header = append(header, latitudeColumn)
						headerIndexMap[latitudeColumn] = len(header)
						extraColumnMap[latitudeColumn] = true
					}
					if _, ok := headerIndexMap[longitudeColumn]; !ok {
						header = append(header, longitudeColumn)
						headerIndexMap[longitudeColumn] = len(header)
						extraColumnMap[longitudeColumn] = true
					}
				}
				if lookupRD {
					if _, ok := headerIndexMap[rdColumn]; !ok {
						header = append(header, rdColumn)
						headerIndexMap[rdColumn] = len(header)
						extraColumnMap[rdColumn] = true
					}
				}
				if lookupSD {
					if _, ok := headerIndexMap[sdColumn]; !ok {
						header = append(header, sdColumn)
						headerIndexMap[sdColumn] = len(header)
						extraColumnMap[sdColumn] = true
					}
				}

				for r, row := range rows {
					rowMap := map[string]string{}
					for c, name := range header {
						if !extraColumnMap[name] {
							rowMap[name] = row[c]
						}
					}

					if lookupCoordinates {
						addressLine := addressFormat
						for _, name := range header {
							addressLine = strings.ReplaceAll(addressLine, "${"+name+"}", rowMap[name])
						}
						logrus.Debugf("Row[%d]: Address line: %s", r, addressLine)

						latitude, longitude, err := gisLookupAddress(addressLine, addressQuality)
						if err != nil {
							logrus.Warnf("Row[%d]: Could not lookup address '%s': %v", r, addressLine, err)
						} else {
							rowMap[latitudeColumn] = fmt.Sprintf("%v", latitude)
							rowMap[longitudeColumn] = fmt.Sprintf("%v", longitude)
						}
					}

					if lookupRD || lookupSD {
						if rowMap[latitudeColumn] == "" || rowMap[longitudeColumn] == "" {
							logrus.Warnf("Row[%d]: No coordinates.", r)
						} else {
							coordinates := rowMap[longitudeColumn] + "," + rowMap[latitudeColumn]

							if lookupRD {
								district, err := gisLookupRD(coordinates)
								if err != nil {
									logrus.Warnf("Row[%d]: Could not lookup RD '%s': %v", r, coordinates, err)
								} else {
									rowMap[rdColumn] = district
								}
							}
							if lookupSD {
								district, err := gisLookupSD(coordinates)
								if err != nil {
									logrus.Warnf("Row[%d]: Could not lookup SD '%s': %v", r, coordinates, err)
								} else {
									rowMap[sdColumn] = district
								}
							}
						}
					}

					row = make([]string, len(header))
					for c, name := range header {
						row[c] = rowMap[name]
					}
					rows[r] = row
				}

				outputHandle := io.Writer(os.Stdout)
				csvWriter := csv.NewWriter(outputHandle)
				csvWriter.Write(header)
				csvWriter.WriteAll(rows)
			},
		}
		cmd.Flags().StringVar(&addressFormat, "address-format", "", "Specify the address using column names within '${}', such as '${Res_Addr_House_No} ${Res_Addr_Street_Direction_Prefix} ${Res_Addr_Street_Name} ${Res_Addr_Street_Type}, ${Res_Addr_City}, ${Res_Addr_State} ${Res_Addr_Zip_Code}'.")
		cmd.Flags().Float64Var(&addressQuality, "address-quality", 80.0, "Only use address search results with a quality score of this value or higher (range is from 0 to 100).")
		cmd.Flags().IntVar(&limit, "limit", 0, "Limit the input to this many rows (use 0 for no limit).")
		cmd.Flags().BoolVar(&lookupCoordinates, "lookup-coordinates", false, "Enable this to lookup the coordinates.")
		cmd.Flags().BoolVar(&lookupRD, "lookup-rd", false, "Enable this to lookup the RD.")
		cmd.Flags().BoolVar(&lookupSD, "lookup-sd", false, "Enable this to lookup the SD.")
		rootCmd.AddCommand(cmd)
	}

	err := rootCmd.Execute()
	if err != nil {
		logrus.Errorf("Error: [%T] %v", err, err)
		os.Exit(1)
	}
}

// Example: https://gis.nccde.org/agsserver/rest/services/Composite_Locator/GeocodeServer/findAddressCandidates?Street=&City=&ZIP=&SingleLine=703+N+ORANGE+ST%2C+WILMINGTON%2C+DE+19801&outFields=&maxLocations=&matchOutOfRange=true&langCode=&locationType=&sourceCountry=&category=&location=&distance=&searchExtent=&outSR=102100&magicKey=&f=pjson
func gisLookupAddress(address string, quality float64) (float64, float64, error) {
	location := "https://gis.nccde.org/agsserver/rest/services/Composite_Locator/GeocodeServer/findAddressCandidates"
	params := url.Values{}
	params.Set("SingleLine", address)
	params.Set("outFields", "*")
	params.Set("maxLocations", "1")
	params.Set("outSR", "4326") // "WGS 84" (latitude, longitude).
	params.Set("f", "pjson")
	location = location + "?" + params.Encode()
	response, err := http.Get(location)
	if err != nil {
		return 0, 0, err
	}
	contents, err := io.ReadAll(response.Body)
	if err != nil {
		return 0, 0, err
	}

	type Output struct {
		Candidates []struct {
			Address  string `json:"address"`
			Location struct {
				X float64 `json:"x"`
				Y float64 `json:"y"`
			}
			Score float64 `json:"score"`
		} `json:"candidates"`
	}
	var output Output
	err = json.Unmarshal(contents, &output)
	if err != nil {
		return 0, 0, err
	}

	if len(output.Candidates) == 0 {
		return 0, 0, fmt.Errorf("no candidates found")
	}

	for _, candidate := range output.Candidates {
		if candidate.Score >= quality {
			return candidate.Location.Y, candidate.Location.X, nil // Remember, latitude is the "y" coordinate and longitude is the "x" coordinate.
		}
	}

	return 0, 0, fmt.Errorf("no quality candidates found")
}

func gisLookupRD(coordinates string) (string, error) {
	return gisLookupPoliticalLayer("2", coordinates)
}

func gisLookupSD(coordinates string) (string, error) {
	return gisLookupPoliticalLayer("1", coordinates)
}

// Example: https://gis.nccde.org/agsserver/rest/services/BaseMaps/Political/MapServer/2/query?where=&text=&objectIds=&time=&timeRelation=esriTimeRelationOverlaps&geometry=-8410299.508534422%2C4828743.515982654&geometryType=esriGeometryPoint&inSR=102100&spatialRel=esriSpatialRelIntersects&distance=&units=esriSRUnit_Foot&relationParam=&outFields=*&returnGeometry=false&returnTrueCurves=false&maxAllowableOffset=&geometryPrecision=&outSR=102100&havingClause=&returnIdsOnly=false&returnCountOnly=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&returnZ=false&returnM=false&gdbVersion=&historicMoment=&returnDistinctValues=false&resultOffset=&resultRecordCount=&returnExtentOnly=false&sqlFormat=none&datumTransformation=&parameterValues=&rangeValues=&quantizationParameters=&featureEncoding=esriDefault&f=pjson
func gisLookupPoliticalLayer(layer string, coordinates string) (string, error) {
	location := "https://gis.nccde.org/agsserver/rest/services/BaseMaps/Political/MapServer/" + url.PathEscape(layer) + "/query"
	params := url.Values{}
	params.Set("geometry", coordinates)
	params.Set("geometryType", "esriGeometryPoint")
	params.Set("inSR", "4326") // "WGS 84" (latitude, longitude).
	params.Set("spatialRel", "esriSpatialRelIntersects")
	params.Set("outFields", "*")
	params.Set("returnGeometry", "false")
	params.Set("outSR", "4326") // "WGS 84" (latitude, longitude).
	params.Set("f", "pjson")
	location = location + "?" + params.Encode()
	response, err := http.Get(location)
	if err != nil {
		return "", err
	}
	contents, err := io.ReadAll(response.Body)
	if err != nil {
		return "", err
	}

	type Output struct {
		Features []struct {
			Attributes map[string]interface{} `json:"attributes"`
		} `json:"features"`
	}
	var output Output
	err = json.Unmarshal(contents, &output)
	if err != nil {
		return "", err
	}

	if len(output.Features) == 0 {
		return "", fmt.Errorf("no features found")
	}
	return fmt.Sprintf("%v", output.Features[0].Attributes["DISTRICT"]), nil
}
